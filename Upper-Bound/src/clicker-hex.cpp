#include <cstdio>
using namespace std;

int main()
{
  int a, b;

  { int a; a = 263; printf("0x%x\n", a); }
  { int a, b; a = 263; b = 3; printf("0x%x\n", a & b); }
  { int a, b; a = 263; b = 3; printf("0x%x\n", a ^ b); }

  return 0;
}
