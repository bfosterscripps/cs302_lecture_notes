#include <cstdlib>
#include <iostream>
using namespace std;

class Fib {               // Step 1 in calculating Fibonacci numbers with Dynamic Programming:
  public:                 // Find a recursive solution, which may be very slow.
    long long find_fib(int n);
};

long long Fib::find_fib(int n)  // Classic recursive implementation straight from the definition.
{
  if (n == 0 || n == 1) return 1;
  return find_fib(n-1) + find_fib(n-2);
}

int main(int argc, char **argv)
{
  Fib f;

  if (argc != 2) { cerr << "usage: fib n\n"; exit(1); }

  cout << f.find_fib(atoi(argv[1])) << endl;
  return 0;
}
