/* Instead of calling LCS on strings, we're going to call it on indices of the two strings,
   which we'll store in the class. */

#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>
using namespace std;

class Subseq {
  public:
    string s1, s2;
    int MCS(size_t i1, size_t i2);
};

/* This code doesn't memoize -- it's like the step 1 code that worked on strings,
   only now we're not making any copies of strings. */

int Subseq::MCS(size_t i1, size_t i2)
{
  int r1, r2;

  if (s1.size() == i1 || s2.size() == i2) return 0;

  if (s1[i1] == s2[i2]) {
    return 1 + MCS(i1+1, i2+1);
  } else {
    r1 = MCS(i1, i2+1);
    r2 = MCS(i1+1, i2);
    return (r1 > r2) ? r1 : r2;
  }
}

int main(int argc, char **argv)
{
  Subseq ss;

  if (argc != 3) {
    cerr << "usage: subseq s1 s2\n";
    exit(1);
  }

  ss.s1 = argv[1];
  ss.s2 = argv[2];

  cout << ss.MCS(0, 0) << endl;
  return 0;
}
  
