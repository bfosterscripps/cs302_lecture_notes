vector <string> puzzle;

bool Is_Row_Ok(int r)
{
  char i;

  for (i = '1'; i <= '9'; i++) {
    if (puzzle[r].count(i) > 1) return false;
  }
  return true;
}
