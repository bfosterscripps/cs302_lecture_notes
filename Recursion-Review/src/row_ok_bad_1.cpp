bool Sudoku::Is_Row_Ok(int r) const
{
  int i, j;

  for (i = 0; i < puzzle[r].size(); i++) {
    if (puzzle[r][i] != '-') {
      for (j = i+1; j < puzzle[r].size(); j++) {
        if (puzzle[r][i] == puzzle[r][j]) return false;
      }
    }
  }
  return true;
}
