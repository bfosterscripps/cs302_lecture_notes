if [ ! -d csv -o ! -d salaries ]; then
  echo "Missing salaries and/or csv directories" >&2
  exit 1
fi

for i in csv/*-Salaries.csv ; do
  stem=`echo $i | sed 's/csv.\(.*\)-Salaries.csv/\1/'`
  ( echo ' { "value": "salary", "tournament": "'$stem'", "golfers": {'
  sed 1d $i | sed '$d' | sed 's/..\(.*\) (.*,G,\([0-9]*\).*/"\1": \2,/'
  sed -n '$p' $i | sed 's/..\(.*\) (.*,G,\([0-9]*\).*/"\1": \2 }} /' ) > salaries/$stem.txt
done
   
