if [ ! -d csv -o ! -d standings ]; then
  echo "Missing standings and/or csv directories" >&2
  exit 1
fi

for i in csv/*-Standings.csv ; do
  stem=`echo $i | sed 's/csv.\(.*\)-Standings.csv/\1/'`
  ( echo ' { "value": "score", "tournament": "'$stem'", "golfers": {'
  sed 1d $i | sed '/,,,/d' | sed '$d' | sed 's/.*,,\(.*\),G,.*,/"\1": /' | sed 's/\(.*\)/\1,/' 
  sed 1d $i | sed '/,,,/d' | sed -n '$p' | sed 's/.*,,\(.*\),G,.*,/"\1": /' | sed 's/\(.*\)/\1 }}/' 
  ) | jdump 2 >  standings/$stem.txt
done
   
