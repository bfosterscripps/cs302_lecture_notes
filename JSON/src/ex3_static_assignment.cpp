/* This program shows how you can assign a json object (or array) statically in your program. */

#include "nlohmann/json.hpp"
#include <iostream>
using namespace std;
using nlohmann::json;

static json j = {
         { "first", "Tiger" },
         { "last", "Woods" },
         { "wins", 82 },
         { "majors", 15 },
         { "masters-wins", { 1997, 2001, 2002, 2005, 2019 } },   // These are arrays.
         { "pga-wins", { 1999, 2000, 2006, 2007 } },
         { "us-open-wins", { 2000, 2002, 2008 } },
         { "british-open-wins", { 2000, 2005, 2006} } };


int main()
{
  cout << j.dump(2) << endl;
}
