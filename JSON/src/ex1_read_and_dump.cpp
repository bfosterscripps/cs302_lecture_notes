/* Read JSON from standard input, and then use dump(2) to write it indented by two spaces. */

#include "nlohmann/json.hpp"
#include <iostream>
using namespace std;
using nlohmann::json;

int main()
{
  json js;

  cin >> js;
  
  cout << js.dump(2) << endl;
  return 0;
}
